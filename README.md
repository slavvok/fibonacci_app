Fibonacci service

**Run application**
```
docker-compose up -d --build
```

**Endpoints**

GET localhost:8000/fibonacci?from={from}&to={to} Returns json with args [from, to, range]
    where 'range' presents fibonacci sequence in range ['from':'to']

**Developing and testing**

Run redis in container
```commandline
sudo docker run -d -p 6379:6379 --name redis --hostname redis redis
```

Install requirements
```commandline
pip install -r requirements.txt
```

Run app
```commandline
python wsgi.py
```

Test app
```commandline
python tests.py
```
