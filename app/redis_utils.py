from app.settings import conn


class EmptyValue(Exception):
    pass


def fibonacci_task(first: int, last: int) -> list:
    """
    If hash-fibonacci 'first, last' key exists, takes value stored by that key
    Else takes list-fibonacci value, stores new range into hash-fibonacci
    :param first: int
    :param last: int
    :return: list
    """
    if last <= first:
        raise ValueError

    key = f'{first},{last}'

    fib_range = conn.hget('hash-fibonacci', key)

    if fib_range:
        result = fib_range.decode().split(', ')
    else:
        # Take list-fibonacci value from redis
        result = conn.lrange('list-fibonacci', 0, -1)
        result = [int(i) for i in result]
        result = [i for i in result if (i >= first) and (i < last)]
        if not result:
            raise EmptyValue

        result = [str(i) for i in result]
        conn.hset('hash-fibonacci', key=key, value=', '.join(result))

    return result


def setup_fibonacci() -> None:
    """
    Creates list-fibonacci if it doesn't exist
    """
    if not conn.exists('list-fibonacci'):
        lst = list()
        a = 0
        b = 1
        while b < 1000_000_000_000:
            lst.append(str(b))
            a = b
            b += a

        conn.rpush('list-fibonacci', *lst)
