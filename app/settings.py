import os
import redis


REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')

if REDIS_HOST and REDIS_PORT:
    conn = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)
else:
    conn = redis.Redis(host='localhost', port=6379)
