from flask_restful import Resource
from flask import request
from app.redis_utils import fibonacci_task
from ..redis_utils import EmptyValue


class FibonacciResource(Resource):
    def get(self):
        try:
            first = int(request.args['from'])
            last = int(request.args['to'])
            result = fibonacci_task(first, last)
        except ValueError:
            return {'message': 'Wrong values'}, 400
        except EmptyValue:
            return {'message': 'Empty resulting range'}, 400
        else:
            return {
                       'from': first,
                       'to': last,
                       'range': result
                   }, 200
