from flask import Flask
from flask_restful import Api
from .resources.fibonacci import FibonacciResource
from .redis_utils import setup_fibonacci


app = Flask(__name__)
fib_api = Api(app)
fib_api.add_resource(FibonacciResource, '/fibonacci')


def init_app():
    # Test method
    app = Flask(__name__)
    app.config['TESTING'] = True
    fib_api = Api(app)
    fib_api.add_resource(FibonacciResource, '/fibonacci')
    return app


setup_fibonacci()
