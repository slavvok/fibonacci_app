from app import init_app
from app.settings import conn
import unittest


class FibonacciRedisTestCase(unittest.TestCase):
    def setUp(self) -> None:
        app = init_app()
        self.app = app.test_client()
        app.app_context().push()

        # Params
        self.first = 1
        self.last = 100_000

    def tearDown(self) -> None:
        conn.delete('list-fibonacci', 'hash-fibonacci')

    def test_status_code(self):
        client = self.app.get(f'/fibonacci?from={self.first}&to={self.last}')
        assert client.status_code == 200

    def test_first_more_than_last(self):
        client = self.app.get(f'/fibonacci?from={self.last}&to={self.first}')
        assert client.status_code == 400

    def test_no_params(self):
        client = self.app.get(f'/fibonacci')
        assert client.status_code == 400


if __name__ == '__main__':
    unittest.main()
